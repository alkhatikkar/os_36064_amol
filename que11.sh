#!/bin/bash

echo -n " > Enter basic Salary : "
read salary

da=$(echo "scale=4; $salary * 0.4" | bc )
hra=$(echo "scale=4; $salary * 0.2" | bc )

gsalary=$(echo "$salary + $da + $hra" | bc)

echo ""
echo " > Your Gross Salary is : $gsalary"

