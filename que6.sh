#!/bin/bash

echo "		Leap Year"
echo ""

echo -n " > Enter Year : "
read y

if [ `expr $y % 4` -eq 0 -a `expr $y % 100` -ne 0 -o `expr $y % 400` -eq 0 ]
then 
	echo "$y is a Leap Year"
else
	echo "$y isnot a Leap Year"
fi
