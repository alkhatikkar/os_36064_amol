#!/bin/bash

echo -n "Enter Number Limit : "
read n
r1=0
r2=1
r3=0
for ((i=0; i<=n; i++))
do 
	r3=`expr $r1 + $r2`
	echo -n "$r3 "
	r1=$r2
	r2=$r3
done
