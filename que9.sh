#!/bin/bash

echo -n " > Enter Number : "
read n

res=1
for ((i=n; i>0; i--))
do
	res=`expr $res \* $i`
done

echo -e " > Factorial of $n is $res"
